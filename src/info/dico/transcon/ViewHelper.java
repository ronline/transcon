package info.dico.transcon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Button;

public class ViewHelper extends Activity {
	//Pass the relevant Context to the class 
    private Context mContext;
    Intent intent = getIntent();

    //Pass relevant Context in the constructor and keep a reference to the Context as a class variable.
	ViewHelper (Context context) {
	    mContext = context;
	}
	
    /**
     * Switching a view and sending the parameters 
     * @param extra parameter name ex : "PLAYLIST_NAME"
     * @param extraData parameter variable
     * @param cls class name to switch
     */
    void SwitchView(String extra, String extraData, Class<?> cls){
    	Intent intent = new Intent(mContext.getApplicationContext(), LessonsListing.class);
        intent.putExtra(extra, extraData);
        mContext.startActivity(intent);
    }
    

    /**Controller sub functions*/
    /**Player Controllers*/
    
    //Play and Pause
    void playerLabel(Button playerBtn, MediaPlayer mp){
        if(mp.isPlaying()) {
        	playerBtn.setText(R.string.play_str);mp.stop();} else {playerBtn.setText(R.string.pause_str);mp.start();}        
        }   
    
    /**
     * Striping the file extension from file name
     * @param elementName
     * @return name without extension
     */
    String stripExtension(String findName){
    	findName = findName.substring(0, findName.lastIndexOf("."));
    	return findName;
    }
	
}
