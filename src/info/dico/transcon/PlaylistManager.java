package info.dico.transcon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;

public class PlaylistManager extends Activity {
	ControllerHelper controllerHelper = new ControllerHelper(this);
	private String lessonNameId;
	ListView listView;
	

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playlist_manager);
        listView = (ListView) findViewById(R.id.pList);
        ArrayList<String> data = null;
        Bundle extras = getIntent().getExtras();
        Button cPlistBtn = (Button)findViewById(R.id.createPlistBtn);
        final EditText newPlist = (EditText)findViewById(R.id.createPlist);
        lessonNameId = extras.getString("LESSON_NAME_ID");
     
        //User decides to create a new playlist
        cPlistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	//text content checking 
            	String newPlistNm = newPlist.getText().toString();
            	//cleaning the textbox
            	newPlist.setText("");
            	if(newPlistNm.matches("")){
                	//Inserting new playlist and lesson to the db
            		Toast.makeText(getApplicationContext(), "Please fill the text box", 10).show();
            		}
            	else
            	{
            		Integer pListInsertId = 0;
            		//The autoincrement in playlist table doesn't work, shame on me or sqlite ?  ...
            		try{
            		pListInsertId = Integer.parseInt(controllerHelper.getIdByName("","insertId"));
            		pListInsertId = pListInsertId + 1;}
            		catch (java.lang.NumberFormatException exception){
            			pListInsertId = 1; 
            		}
            		//inserting new playlist, lesson id and unused user id
            	    controllerHelper.insertContent("playlist", "" + pListInsertId.toString() + ", '" + newPlistNm.toString() + "'" + "," + 0);
            	    //inserting new playlist and lesson ids into jonction table
            	    controllerHelper.insertContent("lesson_shows_in_playlist", "" + pListInsertId.toString() +", " + lessonNameId);
            	    //inserting the playlist id and lesson id to 
            		Toast.makeText(getApplicationContext(), " Lesson Saved in  " + newPlistNm + "  " , 10).show();
                	//redirecting back home
                    Intent intent = new Intent(getApplicationContext(), Home.class);
            		//closing db connection before destroying the activity
            		if (DbHelper.db != null) { DbHelper.db.close();}
                    startActivity(intent);
            		}
            	} 
        });
        
        
        //User decides to save the lesson in an existing playlist
        if(extras.getString("LESSON_NAME_ID") != null && extras.getString("PLAYLIST_NAME") != null)
        {
        	String plName;
        	plName = extras.getString("PLAYLIST_NAME");
        	//get the id of the lesson and playlist
        	Integer pListId = Integer.parseInt(controllerHelper.getIdByName(plName, "playlist"));
        	Integer lnId = Integer.parseInt(extras.getString("LESSON_NAME_ID"));
        	try{
            DbHelper.insertContent("INSERT INTO lesson_shows_in_playlist VALUES ('" + pListId +"', '" + lnId  + "')", getBaseContext());
            Toast.makeText(getApplicationContext(), " Lesson Saved in  " + plName + "  " , 10).show();
        	}
        	catch (SQLiteConstraintException exception){
        		Toast.makeText(getApplicationContext(), plName + " already has a lesson with similar name ...  " , 10).show();
        	}
           
            Intent intent = new Intent(getApplicationContext(), PlaylistListing.class);
            startActivity(intent);
        }
        
        if(!DbHelper.isDatabaseExists(getBaseContext())){ DbHelper.importDataBase(getApplicationContext()); }
        data = controllerHelper.titleListing("Playlist", null); 
        //The same mechanism as in PlaylistListing with overridden redirection 
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1, android.R.id.text1, data);
        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) { 
               // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), PlaylistManager.class);
                intent.putExtra("PLAYLIST_NAME", itemValue);
                intent.putExtra("LESSON_NAME_ID", lessonNameId);
                startActivity(intent);
              }
         });
	  }
}
