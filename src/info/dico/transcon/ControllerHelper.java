package info.dico.transcon;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;

//Lesson listing accordingly to selected category  
public class ControllerHelper extends Activity {

    private ArrayList<Integer> listCatImg;  
    private ArrayList<String> listCatName;
    
	//Pass the relevant Context to the class 
    private Context mContext;

    //Pass relevant Context in the constructor and keep a reference to the Context as a class variable.
	ControllerHelper (Context context) {
	    mContext = context;
	}

	/**
	 * Connecting to dbHelper to extract the audio 
	 * @param qr query
	 * @return the string with the name of the audio file 
	 */
	protected  String audioFile(String lessonName){
        String qr = "SELECT DISTINCT audio_content FROM lesson l INNER JOIN lesson_name ln on ln.lesson_name_id = " +
        		"l.lesson_name_lesson_name_id INNER JOIN audio_content ac on l.audio_content_id_audio_content = " +
        		"ac.id_audio_content AND ln.lesson_name = '"+ lessonName + "';";
		Cursor cursor = null;
		cursor = DbHelper.getLessonTitles(qr);
        String audioFile = "";
        //use hasNext() and next() methods of Iterator to iterate through the elements
        for (cursor.moveToFirst(); !cursor.isAfterLast();) { audioFile = cursor.getString(0) +"\n"; break;}
        cursor.close();
		return audioFile;
	}

	/**
	 * Connecting to dbHelper to extract a lesson content
	 * @param qr
	 * @return the lesson content
	 */
	protected  String lessonsView(String lessonName){
		String qr = "SELECT original_content, translation FROM lesson " +
				"l INNER JOIN  lesson_name ln on ln.lesson_name_id = l.lesson_name_lesson_name_id " +
        		"INNER JOIN audio_content ac on l.audio_content_id_audio_content = ac.id_audio_content " +
        		"AND ln.lesson_name = '"+ lessonName +"' ORDER BY lesson_name_id;"; 
		Cursor cursor = null;
		cursor = DbHelper.getLessonTitles(qr);
        String lesson = "";
        //use hasNext() and next() methods of Iterator to iterate through the elements
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) 
        //{lesson = lesson + cursor.getString(1) + cursor.getString(2) +"\n\n";}
        {
        	lesson = lesson + cursor.getString(0) + 
        	cursor.getString(1) + "\n\n";
        }
        cursor.close();
		return lesson;
	}
	
	/**
	 * Connecting to dbHelper to extract the lessons titles 
	 * @param category lesson category
	 * @param qr
	 * @return the array with lesson titles
	 */
	protected  String lessonNameId(String lessonName){
		String qr = "SELECT lesson_name_lesson_name_id FROM lesson l" +
				" INNER JOIN  lesson_name ln on ln.lesson_name_id = l.lesson_name_lesson_name_id " +
        		"INNER JOIN audio_content ac on l.audio_content_id_audio_content = ac.id_audio_content " +
        		"AND ln.lesson_name = '"+ lessonName +"' GROUP BY lesson_name_lesson_name_id;";
		String res = null;
		res = DbHelper.getSingleRow(qr);
		return res;
	}
	
	
	/**
	 * Connecting to dbHelper to extract the lessons titles 
	 * @param category lesson category
	 * @param qr
	 * @return the array with lesson titles
	 */
	protected  String getIdByName(String name, String sw){
		String qr = null;
		if(sw.equals("playlist")) qr = "SELECT playlist_id FROM playlist p  where p.playlist_name = '"+ name +"'";
		if(sw.equals("insertId")) qr = "SELECT MAX(playlist_id) FROM playlist";
		String res = null;
		res = DbHelper.getSingleRow(qr);
		return res;
	}

	/**
	 * Inserting content through a simple sql query
	 * @param sw switch to select query
	 */
	protected void insertContent(String sw, String values){ 
		String qr = null;
		qr = "INSERT into '" + sw + "' VALUES(" + values + ");";
		DbHelper.insertContent(qr, mContext);
	}
	
	/**
	 * Deleting a content from db
	 * @param idToDel id of element
	 * @param tbl table to deal with
	 */
	protected void deleteContent(int idToDel, String tbl, String elementId){
		String qr = null;
		qr = "DELETE FROM '" + tbl + "' WHERE " + elementId  + " = " + idToDel + ";";
		DbHelper.execQuery(qr);
	}
	
	
	/**
	 * Connecting to dbHelper to extract the lessons titles 
	 * @param category lesson category
	 * @param qr
	 * @return the array with lesson titles
	 */
	protected  ArrayList<String> titleListing(String choice, String cat){
		String qr = null;
		//Playlist titles
		if(cat == null && choice.equals("Playlist")) qr ="SELECT playlist_name playlist_id FROM playlist";
		//Lesson titles
		if(cat != null && choice.equals("Lessons")) qr ="SELECT DISTINCT lesson_name FROM lesson_name ln  " +
				"INNER JOIN lesson l ON ln.lesson_name_id = l.lesson_name_lesson_name_id INNER JOIN groups g " +
				"ON  l.groups_groups_id = g.groups_id WHERE group_name like \"%" + cat  +"%\" ORDER BY ln.lesson_weight ASC";
		//Lessons per playlist
		if(cat != null && choice.equals("Playlist-Lessons")) qr ="SELECT ln.lesson_name, p.playlist_name  FROM lesson_name ln " +
				"INNER JOIN lesson_shows_in_playlist lsp ON lsp.lesson_name_lesson_name_id = ln.lesson_name_id INNER JOIN " +
				"playlist p on p.playlist_id = lsp.playlist_playlist_id where p.playlist_name = \"" + cat  + "\"";
		Cursor c = null;
		ArrayList<String> data = new ArrayList<String>();
		c = DbHelper.getLessonTitles(qr);
        c.moveToFirst();while(!c.isAfterLast()) {
            data.add(c.getString(c.getShort(0)));
            c.moveToNext();
       }
            c.close();
		return data;
	}

	
	
	/**
	 * Connecting to dbHelper to extract the lessons titles 
	 * @param category lesson category
	 * @param qr
	 * @return the array with lesson titles
	 */
	protected  ArrayList<String> lessonsListing(String qr){
		Cursor c = null;
		ArrayList<String> data = new ArrayList<String>();
		c = DbHelper.getLessonTitles(qr);
        c.moveToFirst();while(!c.isAfterLast()) {
            data.add(c.getString(c.getShort(0)));
            c.moveToNext();
       }
            c.close();
		return data;
	}
	
	/**
	 * Populating adapter with names and image 
	 * @param ga 
	 * @param cat image and name category 
	 * @return array filtered by category
	 */
	protected GridviewAdapter ShowContent(GridviewAdapter ga, String cat){
		listCatImg = prepareListImg(cat);
		listCatName = prepareListName(cat);
		Activity activity = (Activity) mContext;
		ga = new GridviewAdapter(activity,listCatName, listCatImg);  
		return ga;
	}
	
	  /**
	   * Switching pages
	   * 
	   * @param cls class file name
	   * @param ga GridviewAdapter to populate the view
	   * @param position name of clicked element
	   */
	  public void SwitchActivityPage(Class<?> cls, GridviewAdapter ga, int position){
	      Intent intent = new Intent(mContext.getApplicationContext(), cls);
		  String pos = ga.getItem(position);
		  intent.putExtra("id", pos);
		  mContext.startActivity(intent);
	  }
	  
	    //reloading the same view
	    void SwitchView(String extra, String extraData, Class<?> cls){
	    	//Intent intent = new Intent(mContext.getApplicationContext(), cls);
	    	Intent intent = new Intent(mContext.getApplicationContext(), LessonsListing.class);
	        intent.putExtra(extra, extraData);
	        mContext.startActivity(intent);
	        }

	  /**
	   * A "hack helper" function for the "Orders" group
	   * In fact this group doesn't have any category and
	   * instead of showing and empty page I am redirecting 
	   * user to lesson listing who belongs to orders group
	   * The method may be deleted when category orders will be created 
	   * @param position of clicked array item
	   */
	  public void goToListing(int position){
  		GridviewAdapter lstAdapter = null;
    	lstAdapter = ShowContent(lstAdapter, "Orders");
		SwitchActivityPage(LessonsListing.class, lstAdapter, position);
	  }	
	  
	/**
	 * Preparing the list with category names
	 * @param choice category group
	 * @return populated array
	 */
	
	   public ArrayList<String> prepareListName(String choice)  
	   {  
		   ArrayList<String> listName;
		     listName = new ArrayList<String>();
		     
		     if(choice.equals("Default")){
	         listName.add("Orders");
		     listName.add("Maintenance");
	         listName.add("Confrontation");    
	         listName.add("Transport");      
	         listName.add("Playlist");     
		     }

		     if(choice.equals("Maintenance")){
		         listName.add("Vehicles");    
			     listName.add("Weapons");     
			 }

		     if(choice.equals("Confrontation")){
		         listName.add("Checkpoint");    
			     listName.add("Action");
			     listName.add("Orientation");
			     listName.add("Injury");     
			 }

		     if(choice.equals("Transport")){
		         listName.add("Organizing");    
			     listName.add("Navigation");     
			 }
		     
		     if(choice.equals("Orders")){
		         listName.add("Orders");     
			 }
		   
	         return listName;
	   }     
	   
	   /**
	    * Preparing list of images
	    * 
	    * @param choice = category group
	    * @return array of images
	    */
	   public ArrayList<Integer> prepareListImg(String choice)
	   {
		    ArrayList<Integer> listImg;  
	        listImg = new ArrayList<Integer>();
	        
	        if(choice.equals("Default"))
	        {
	         listImg.add(R.drawable.orders);    
	         listImg.add(R.drawable.maintenance);  
	         listImg.add(R.drawable.confrontation);  
	         listImg.add(R.drawable.transport);
	         listImg.add(R.drawable.playlist);
	        }

	        if(choice.equals("Maintenance"))
	        {
	         listImg.add(R.drawable.vehicles);    
	         listImg.add(R.drawable.weapons);
	        }

	        if(choice.equals("Confrontation"))
	        {
		         listImg.add(R.drawable.checkpoint);    
		         listImg.add(R.drawable.action);
		         listImg.add(R.drawable.orientation);    
		         listImg.add(R.drawable.injuries);
	        }	        	        

	        if(choice.equals("Transport"))
	        {
		         listImg.add(R.drawable.organizing);    
		         listImg.add(R.drawable.navigation);
	        }
	        	        
	        if(choice.equals("Orders"))
	        {
		         listImg.add(R.drawable.orders);
	        }
	        	        
	        
	        return listImg;
	   }
}
