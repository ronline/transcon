package info.dico.transcon;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreatePlaylist extends Activity {

	ControllerHelper controllerHelper = new ControllerHelper(this);
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_playlist);
        EditText createPl = (EditText) findViewById(R.id.createPlist);
        Button createPlBt = (Button) findViewById(R.id.createPlistBt);
        Bundle extras = getIntent().getExtras();
        final String lessonNameId = extras.getString("LESSON_NAME_ID");
        

        Toast.makeText(getApplicationContext(), "hi create playlist " + lessonNameId, 10).show();
        
        createPlBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "hi from text " + lessonNameId, 10).show();
            }
        });
	}

}
