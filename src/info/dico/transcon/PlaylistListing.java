package info.dico.transcon;

import java.util.ArrayList;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;	
import android.widget.Toast;

//Lesson listing accordingly to selected category  
public class PlaylistListing extends ListActivity {

	//ListView listView;
	ControllerHelper controllerHelper = new ControllerHelper(this);
	ViewHelper viewHelper = new ViewHelper(this);
  
	
   @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<String> data = null;
        Intent intent = getIntent();
        setContentView(R.layout.playlist_background);
        
        if ( intent.hasExtra("PLAYLIST_NAME")) { 
        	controllerHelper.SwitchView("PLAYLIST_NAME", intent.getStringExtra("PLAYLIST_NAME"), LessonsListing.class); }
        if(!DbHelper.isDatabaseExists(getBaseContext())){ DbHelper.importDataBase(getApplicationContext()); }
        data = controllerHelper.titleListing("Playlist", null);
        
        setListAdapter(new PlaylistArrayAdapter(getApplicationContext(), data));
        }
   
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String selectedValue = (String) getListAdapter().getItem(position);
		Intent intent = new Intent(getApplicationContext(), PlaylistListing.class);
		intent.putExtra("PLAYLIST_NAME", selectedValue);
		//closing db connection before destroying the activity
		if (DbHelper.db != null) { DbHelper.db.close();}
		startActivity(intent);
	}
	
   //for some reason was the back key turning in a loop on this page ..
   @Override
   public boolean onKeyDown(int keyCode, KeyEvent event) {
       if( keyCode == KeyEvent.KEYCODE_BACK) {
    	   Intent intent = new Intent(getApplicationContext(), Home.class);
    	   startActivity(intent);
       }
	return false;
   }
}
