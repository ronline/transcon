package info.dico.transcon;

import android.app.ListActivity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

//Called from Listing class, file will get lesson id parameter
public class LessonView extends ListActivity implements OnClickListener, OnTouchListener, OnCompletionListener, Runnable  {
	

	private	 MediaPlayer mediaPlayer;
	
	private TextView lessonTxt;
	private String lessonName;
	private String audioFile;
	private String lessonNameId;
	private Button playerBtn;
	private SeekBar seekBar;
	private final Handler handler = new Handler();
	ControllerHelper controllerHelper = new ControllerHelper(this);
	ViewHelper viewHelper = new ViewHelper(this);
	CheckBox addToPl;
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lessons);    
        lessonTxt = (TextView) findViewById(R.id.lesson);
        Bundle extras = getIntent().getExtras();
        if (extras != null) { lessonName = extras.getString("LESSON_NAME");}
        String lesson = controllerHelper.lessonsView(lessonName);
        audioFile = controllerHelper.audioFile(lessonName);
        lessonNameId = controllerHelper.lessonNameId(lessonName);
        
    	
    	
        /** audio player **/
        //Striping the file extension
        String audioName = viewHelper.stripExtension(audioFile);
        //getting the media resource
        mediaPlayer = MediaPlayer.create(LessonView.this, Uri.parse("android.resource://" + this.getPackageName() + "/raw/" + audioName));  
        mediaPlayer.setOnCompletionListener(this); 
        playerBtn = (Button)findViewById(R.id.playerBtn);
        playerBtn.setOnClickListener(this);
        seekBar = (SeekBar) findViewById(R.id.PlayerSeekBar);
            seekBar.setMax(mediaPlayer.getDuration());
            seekBar.setOnTouchListener(this);
            //handling screen rotation
            if (savedInstanceState != null) {
            	//restoring the seek bar and audio position 
                mediaPlayer.seekTo(savedInstanceState.getInt("current_position"));
                seekBar.setProgress(savedInstanceState.getInt("current_position"));
                viewHelper.playerLabel(playerBtn, mediaPlayer);
                handler.post(this);
            }
        /** end of audio player **/
        //add lesson checkbox listener
            addLessonToPlist();
        //lesson content    
        lessonTxt.setText(lesson); 
    }
    //add lesson to playlist
    public void addLessonToPlist(){
    	addToPl = (CheckBox) findViewById(R.id.addLsToPl);
    	addToPl.setOnClickListener(new OnClickListener() {
    		 
    		  @Override
    		  public void onClick(View v) {
    			  //this will be used in other view so i will put it into a helper class
                  Intent intent = new Intent(getApplicationContext(), PlaylistManager.class);
                  intent.putExtra("LESSON_NAME_ID", lessonNameId);
          		//closing db connection before destroying the activity
           		if (DbHelper.db != null) { DbHelper.db.close();}
                  startActivity(intent);
    		  }
    		});
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("current_position", mediaPlayer.getCurrentPosition());
        outState.putBoolean("player_state", mediaPlayer.isPlaying());
        handler.removeCallbacks(this);
        if(mediaPlayer.isPlaying()) mediaPlayer.pause();
    }
    @Override public void onClick(View v) { switch (v.getId()) {case R.id.playerBtn:buttonClick();break; default:break;}}
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if ((keyCode == KeyEvent.KEYCODE_BACK)) { mediaPlayer.stop();} return super.onKeyDown(keyCode, event);}
    @Override public void run() { startPlayProgressUpdater(); }
    @Override public void onCompletion(MediaPlayer mp) { mp.seekTo(0); } 
    @Override public boolean onTouch(View v, MotionEvent event) { return false; }
	
    private void buttonClick() {
        if (playerBtn.getText().equals(getString(R.string.play_str).toString())) {
            playerBtn.setText(R.string.pause_str);
            try {
                mediaPlayer.start();
                startPlayProgressUpdater();
            } catch (IllegalStateException e) {
                mediaPlayer.pause();
            }
        } else {
            playerBtn.setText(getString(R.string.play_str));
            mediaPlayer.pause();
            seekBar.setProgress(mediaPlayer.getCurrentPosition()); 
        }
    }

    public void startPlayProgressUpdater() {
        seekBar.setProgress(mediaPlayer.getCurrentPosition());
        //Toast.makeText(getApplicationContext(), getCurrentPosition(), 10)
        if (mediaPlayer.isPlaying()) {
            handler.postDelayed(this, 1000);
        } else if (mediaPlayer.getCurrentPosition() < mediaPlayer.getDuration()) {
            mediaPlayer.pause();
            playerBtn.setText(R.string.play_str);
        }
    }
}
