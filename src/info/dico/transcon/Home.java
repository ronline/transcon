package info.dico.transcon;

import android.app.Activity;  
import android.os.Bundle;  
import android.view.View;  
import android.widget.AdapterView;  
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;
  
public class Home extends Activity {  
   /** Called when the activity is first created. */  
    
	ControllerHelper controllerHelper = new ControllerHelper(this);
    private static GridviewAdapter mAdapter;  
    private static GridView gridView;

  @Override  
   public void onCreate(Bundle savedInstanceState) {  
       super.onCreate(savedInstanceState);  
       setContentView(R.layout.main);
        
        //Building a gridview with text and images
        //for more see showContent method in ControllerHelper class 
        mAdapter = controllerHelper.ShowContent(mAdapter, "Default");     
        gridView = (GridView) findViewById(R.id.gridView1);  
        gridView.setAdapter(mAdapter);   
        
       // Implement On Item click listener  
        gridView.setOnItemClickListener(new OnItemClickListener()  
        {  
            @Override  
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,  
                    long arg3) {
            	// switching directly to lesson listing
            	if(position == 0){controllerHelper.goToListing(position);}
            	//or show categories listing
            	else{ controllerHelper.SwitchActivityPage(CategoriesListing.class, mAdapter, position);}
            }
        });  
    }    
}