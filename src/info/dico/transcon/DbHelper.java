package info.dico.transcon;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
  
public class DbHelper {
	
	private static String DB_NAME = "transcon.db";
	private static String DB_PATH = "/data/data/info.dico.transcon/databases/";
	static SQLiteDatabase db;
	private static String dbPath = DB_PATH + DB_NAME;
	
	//Returning the newly opened database
	private static SQLiteDatabase getDb(){
		return db = SQLiteDatabase.openDatabase(dbPath, null,SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY | SQLiteDatabase.OPEN_READWRITE);
	}
	/**
	 * Creates a empty database on the system and rewrites it with your own database.
	 * Only in the case the database is not already loaded 
	 */
	public static boolean importDataBase(Context context) {
	    InputStream myInput = null;
	    OutputStream myOutput = null;
	    try {
	        // Open local db from assets as the input stream
	        myInput = context.getAssets().open(DB_NAME);
	        createEmptyDatabase(context); // See this method below
	        // Open the empty db as the output stream
	        myOutput = new FileOutputStream(getDatabaseFile(context));

	        // transfer bytes from the inputfile to the outputfile
	        byte[] buffer = new byte[1024];
	        int length; 
	        while ((length = myInput.read(buffer)) > 0) {
	            myOutput.write(buffer, 0, length);
	        }

	        // Close the streams
	        myOutput.flush();
	        myInput.close();
	        myOutput.close();
	        return true;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return false;	
}

	/**
	 * Check if the database already exists.
	 * @return true if it exists, false if it doesn't
	 */
	public static boolean isDatabaseExists(Context context) {
	    return getDatabaseFile(context).exists();
	}

	private static File getDatabaseFile(Context context) {
	    return context.getDatabasePath(DbHelper.DB_NAME);
	}

	/**
	 * Create an empty database into the default application database
	 * folder. So we are going be able to overwrite that database with our database
	 */
	private static void createEmptyDatabase(Context context) {
	            // use anonimous helper to create empty database
	    new SQLiteOpenHelper(context, DbHelper.DB_NAME, null, 1) {
	                    // Methods are empty. We don`t need to override them
	        @Override
	        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        }

	        @Override
	        public void onCreate(SQLiteDatabase db) {
	        }
	    }.getWritableDatabase().close();
	}
	
	static String getSingleRow(String qr){
		SQLiteDatabase db = getDb();
        Cursor cursor = null;
        String element = "";
        try{
            cursor = db.rawQuery(qr, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                element = cursor.getString(0);
            }

            return element;
        }finally {cursor.close();}
	}
	 
	 public static Cursor getLessonTitles(String qr) throws SQLException{
		//NO_LOCALIZED_COLLATORS suffix to get rid of no such table: android_metadata error
		db = getDb();
	    Cursor c = db.rawQuery(qr, null);
	    return c;
		}
	 
	static void insertContent(String qr, Context c) throws SQLException{
		db = getDb();
		db.execSQL(qr);
		db.close(); 
	 }
	
	/**
	 * Deleting or inserting db content 
	 * @param query
	 */
	static void execQuery(String qr) throws SQLException{
		db = getDb();
		db.execSQL(qr);
		db.close();
	}
	
}