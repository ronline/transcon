package info.dico.transcon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

//Lesson listing accordingly to selected category  
public class CategoriesListing extends Activity {

	ControllerHelper controllerHelper = new ControllerHelper(this);
    private static GridviewAdapter lstAdapter;  
    private static GridView lstGridView;
	
   @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Intent intent = getIntent();
        final String category = intent.getStringExtra("id");

        // Playlist is a special case with no category listing
        // another activity will deal whit this
        if(category.equals("Playlist")){
	        Intent intentPl = new Intent(this.getApplicationContext(), PlaylistListing.class);
	  		this.startActivity(intentPl);
        }
        
        // Building a gridview with text and images
        // for more see showContent method in ControllerHelper class 
        lstAdapter = controllerHelper.ShowContent(lstAdapter, category);
        lstGridView = (GridView) findViewById(R.id.gridView1);  
        lstGridView.setAdapter(lstAdapter); 
        
        // Implement On Item click listener 
        lstGridView.setOnItemClickListener(new OnItemClickListener()  
        {  
            @Override  
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,  
                    long arg3) {
            	//Helper activity switcher 
            	controllerHelper.SwitchActivityPage(LessonsListing.class, lstAdapter, position);
            }
        });   
    }
}
