package info.dico.transcon;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 

public class PlaylistArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final ArrayList<String> values;
 
	public PlaylistArrayAdapter(Context context, ArrayList<String> values) {
		super(context, R.layout.custom_list, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final String positionVal = values.get(position);
 
		View rowView = inflater.inflate(R.layout.custom_list, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.txt);
		textView.setText(positionVal);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.ic_menu_delete);
		rowView.findViewById(R.id.ic_menu_delete).setTag(positionVal);
		
        //Triggering the playlist delete 
	    imageView.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	          ControllerHelper controllerHelper = new ControllerHelper(context);
	          //getting id of the playlist
	          int plistId = Integer.parseInt(controllerHelper.getIdByName(positionVal, "playlist"));
	          //deleting the playlist and attached lessons
	          controllerHelper.deleteContent(plistId, "lesson_shows_in_playlist", "playlist_playlist_id" );
	          controllerHelper.deleteContent(plistId, "playlist", "playlist_id");
	          Toast.makeText(context, positionVal + " deleted",Toast.LENGTH_SHORT).show();
				//closing db connection before destroying the activity
				if (DbHelper.db != null) { DbHelper.db.close();}
	            //refreshing the activity
	            Intent redirect = new Intent(context,PlaylistListing.class);
	            redirect.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	            context.startActivity(redirect); 
	        }
	      });

		String s = values.get(position);
		System.out.println(s);
		imageView.setImageResource(R.drawable.ic_menu_delete);
		
		return rowView;
	}
}