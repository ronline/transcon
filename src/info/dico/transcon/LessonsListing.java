package info.dico.transcon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;	
import android.widget.AdapterView.OnItemClickListener;

//Lesson listing accordingly to selected category  
public class LessonsListing extends Activity {

	ListView listView ;
	ControllerHelper controllerHelper = new ControllerHelper(this);
	ArrayList<String> data = null;
    
   @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_lessons_list); 
        listView = (ListView) findViewById(R.id.list);
        
        //Importing database from assets 
        if(!DbHelper.isDatabaseExists(getBaseContext())){ DbHelper.importDataBase(getApplicationContext()); } 
        
        Intent intent = getIntent();
        final String category = intent.getStringExtra("id");
        
        if ( intent.hasExtra("PLAYLIST_NAME")) { 
        	String plName = intent.getStringExtra("PLAYLIST_NAME");
    	    data = controllerHelper.titleListing("Playlist-Lessons", plName);
        }
        else 
        {data = controllerHelper.titleListing("Lessons", category);}

		
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, android.R.id.text1, data);
        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) { 
               // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), LessonView.class);
                intent.putExtra("LESSON_NAME", itemValue);
        		//closing db connection before destroying the activity
        		if (DbHelper.db != null) { DbHelper.db.close();}
                startActivity(intent);
              }
         }); 
    }   
   
}
